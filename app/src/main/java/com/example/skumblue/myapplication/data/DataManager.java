package com.example.skumblue.myapplication.data;

import android.content.Context;

import com.example.skumblue.myapplication.R;
import com.example.skumblue.myapplication.models.Course;

import java.util.ArrayList;


/**
 * Created by skumblue on 05/03/2015.
 */
public class DataManager {
    private static DataManager dataManager;
    private ArrayList<Course> courses;

    public static DataManager getInstance(){
        if(dataManager == null){
            dataManager = new DataManager();
        }
        return dataManager;
    }

    public ArrayList<Course> getData(Context context){
        if(courses == null){
            courses = new ArrayList<Course>();
            courses.add(new Course( context.getString(R.string.curso1title), context.getString(R.string.curso1description), context.getDrawable(R.drawable.curso1drawable), context.getString(R.string.curso1url)  ) );
            courses.add(new Course( context.getString(R.string.curso2title), context.getString(R.string.curso2description), context.getDrawable(R.drawable.curso2drawable), context.getString(R.string.curso2url)  ) );
            courses.add(new Course( context.getString(R.string.curso3title), context.getString(R.string.curso3description), context.getDrawable(R.drawable.curso3drawable), context.getString(R.string.curso3url)  ) );
            courses.add(new Course( context.getString(R.string.curso4title), context.getString(R.string.curso4description), context.getDrawable(R.drawable.curso4drawable), context.getString(R.string.curso4url)  ) );
            courses.add(new Course( context.getString(R.string.curso5title), context.getString(R.string.curso5description), context.getDrawable(R.drawable.curso5drawable), context.getString(R.string.curso5url)  ) );
            courses.add(new Course( context.getString(R.string.curso6title), context.getString(R.string.curso6description), context.getDrawable(R.drawable.curso6drawable), context.getString(R.string.curso6url)  ) );

        }

        return courses;
    }
 }
