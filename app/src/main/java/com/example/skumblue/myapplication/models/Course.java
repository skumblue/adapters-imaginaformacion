package com.example.skumblue.myapplication.models;

import android.graphics.drawable.Drawable;

/**
 * Created by skumblue on 05/03/2015.
 */
public class Course {
    private String title;
    private String description;
    private Drawable image;
    private String url;

    public Course(String title, String description, Drawable image, String url) {
        this.title = title;
        this.description = description;
        this.image = image;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Drawable getImage() {
        return image;
    }

    public String getUrl() {
        return url;
    }
}
