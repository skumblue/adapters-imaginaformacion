package com.example.skumblue.myapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.skumblue.myapplication.R;
import com.example.skumblue.myapplication.models.Course;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by skumblue on 05/03/2015.
 */
public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.ViewHolder> {
    private ArrayList<Course> courses;
    private int itemCourse;
    private Context context;

    public CourseAdapter(Context context, ArrayList<Course> courses, int itemCourse ) {
        this.context = context;
        this.courses = courses;
        this.itemCourse = itemCourse;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from( viewGroup.getContext() ).inflate(itemCourse, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final Course course = courses.get(i);
        viewHolder.title.setText( course.getTitle() );
        viewHolder.description.setText( course.getDescription() );
        viewHolder.image.setImageDrawable(course.getImage());
        viewHolder.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchBrowser(course.getUrl());
            }
        });
        viewHolder.itemView.setTag(course);//?
    }

    public void launchBrowser(String url){
	    Toast.makeText(context, url, Toast.LENGTH_LONG).show();
	    /*
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        context.startActivity(intent);*/
    }


    @Override
    public int getItemCount() {
        return courses.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView title;
        private TextView description;
        private ImageView image;
        private TextView info;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title_course);
            description = (TextView) itemView.findViewById(R.id.description_course);
            image = (ImageView) itemView.findViewById(R.id.image_course);
            info = (Button) itemView.findViewById(R.id.button_course);
        }
    }
}
